﻿Release Notes:
[FIX] NullPointer when closing client before it was loaded.
[FIX] NullPointer at startup regarding server hosts.
[FIX] IllegalThreadStateException when time is set multiple times.
[FIX] Client doesn't crash when an unimplemented language is selected.
[FIX] NPE's no longer appear when a file is not found (related to above).
[FIX] Nullpointer on lvl up at refresh party call.
[FIX] Non-available servers are no longer clickable.
[FIX] Client-sided movement should be smooth and fixed now.
[FIX] Potions are now in the right backpack.
[FIX] Giovani (8th gym leader in kanto) now gives the correct badge.
[FIX] Move buttons now get disabled correctly when switching pokemon.
[FIX] SpeechFrame no longer leaves a ghost behind. (It doesn't with 1 or 2. But does with more than 2)
[FIX] Total experience is now correctly displayed in the Pokemon window.
[FIX] Database is updated after the logout.
[FIX] Pokeballs(except the pokeball) have higher catch rates
[FIX] Happiness is reduced by 10% when defeated in battle

[ADD] Added Logout button to the UI.
[ADD] Serverlist is retrieved from the web.
[ADD] Double Escape doesn't close the game, Esc + Enter now does. Esc cancels quitting.
[ADD] Back buttons on the login screen and server select screen.
[ADD] Cheatdetection system, since the client is open source we need a cheatdetection system on the server side.
[ADD] Black background instead of the loading screen when inside the game.
[ADD] Pokemon movetype is now displayed in the battle interface.
[ADD] More than awesome Bulbasaur spritesheet for character.
[ADD] Hotkeyed pokemon attacks to buttons 1-2-3-4
[ADD] Hotkeyed OLD ROD to 'R', GOOD ROD to 'T', GREAT ROD to 'Y, ULTRA ROD to 'U'
[ADD] keys.ini file. You can set some custom controls here! It's located at res/keys.ini
[ADD] Trainers now give more base gold if you beat them.
[ADD] Friendlist is stored into database, further implementation follows.
[ADD] Evolution stones should work, has been tested with Pikachu.
[ADD] New loading screen.
[ADD] New login screen.
[ADD] New icons for the HUD bar.
[ADD] spriteloader for future addons (like having pokemon walk behind you)
[ADD] EXP bar in battle view

[BUG] NPE when clicking back immidiatly after selecting server (due to that the packetgenerator object hasn't been initialized yet)
[BUG] Both pokemon use struggle when a berry is used during battle.
[BUG] On relog items get incremently multiplied by the quantity that they actually have. 1x10 -> relog -> 2x10 -> relog -> 3x10. This effect is only visually.
[BUG] Clicking an empty slot after winning/losing a fight disconnects the player from the server.

-------------------------------RN 1.4------------------------------------
[ADD] 	Ingame pokedex
[ADD] 	Pokemonium Mall
[ADD] 	All pokeballs (except for gen 4's Park ball and gen 4's Dream Ball) have been implemented. 
	You can buy them in the pokemonium mall
[ADD]	evs
[ADD]	Add original trainer name on pokemon info
[ADD]	Add moves on pokemon info (party-viewer)
[ADD]	The server now records what ball was used to catch a pokemon (all already caught pokemon have been assigned a regular pokeball). For future purpose.

[FIX] 	Movement has been fixed an is way smoother now!
[FIX]	No more loading between maps.
[FIX] 	Client doesn't crash anymore when giving an item to a pokemon.
[FIX] 	One can now press 'Enter' to log out (confirm the dialogue).


//REQUESTS/TODO
[REQ]Hoenn and Sinnoh
[REQ]trainer cards
[REQ]quests
[REQ]battle animations
[REQ]finish kanto (route 20 and safari zone(being worked on by light adept?))
[REQ]tm fly and strength
[REQ]Make names clickable on right screen of pokedex.


*These points have to be verified.